import os, openai
from llama_index import StorageContext, load_index_from_storage

def chatbot(texto_entrada):
    my_api_key = os.environ.get('OPENAI_API_KEY')
    openai.api_key = my_api_key
    storage_context = StorageContext.from_defaults(persist_dir='../modelo')
    modelo = load_index_from_storage(storage_context)
    query_engine = modelo.as_query_engine()
    resp = query_engine.query(texto_entrada)
    respuesta = str(resp)
    return respuesta

def test_entrenamiento():
    ''' Unit Test para la funcion entrenamiento,
        si se entreno de manera correcta crea la
        carpeta "modelo". '''
    assert os.path.exists('../modelo')

def test_chatbot():
    ''' Unit Test para la funcion prueba,
        el chatbot debe dar siempre una 
        respuesta en forma de un string, 
        y tampoco puede quedar en blanco 
        el output prompt. '''
    
    textoentrada = "Que es un zootecnista?"
    # Llama a la función del chatbot con la entrada de prueba
    respuesta = chatbot(textoentrada)
    # Aserciones para verificar el comportamiento esperado de la función del chatbot
    assert isinstance(respuesta, str), 'La respuesta debe ser string.'
    assert respuesta != '', 'The response should not be empty.'
