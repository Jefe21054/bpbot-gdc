# Use la imagen oficial de TensorFlow en Docker como imagen base
FROM tensorflow/tensorflow:latest

# Establece el directorio de trabajo dentro del contenedor
WORKDIR /app

# Copia archivos del proyecto
COPY . /app

# Instala dependencias del proyecto
RUN pip install --no-cache-dir -r requirements.txt

# Expone el puerto en el que se ejecutará Gradio (por defecto es 7860)
EXPOSE 7860

# Set the entry point for your Flask app
CMD ["python3", "test.py"] 