# BPBot - Futuros Líderes - Generación del Cambio
Chatbot personalizado como proyecto de los Trainees de la iniciativa Futuros Líderes - Generación del Cambio, construido con Python y la API de ChatGPT.
## Cómo empezar

Para empezar con este proyecto de chatbot, sigue las siguientes instrucciones.

### Prerequisitos

- Python 3.x.x
- Python Virtual Environment

### Configuración

1. Clona este repositorio en tu máquina local:

   ```bash
   git clone https://gitlab.com/Jefe21054/bpbot-gdc.git
   ```

2. Crea un nuevo entorno virtual Python:

   ```bash
   python3 -m venv venv
   ```

3. Activa el entorno virtual:

   - Linux y Mac:
   ```bash
   source venv/bin/activate
   ```

   - Windows:
   ```cmd
   venv\Scripts\activate
   ```

4. Instala las dependencias necesarias:

   ```bash
   pip install -r requirements.txt
   ```

5. Configura las credenciales de la API OpenAI en un archivo `.env`. Deberías tener algo como esto:

   ```
   OPENAI_API_KEY='YOUR_OPENAI_API_KEY'
   ```

6. Inicia la instancia de entrenamiento del ChatBot:

   ```bash
   python train.py
   ```

7. Espera a que se creen los nuevos archivos necesarios. Para interactuar con el ChatBot necesitarás ejecutar la interfaz de Gradio.

## Ejecución del ChatBot en un navegador con Gradio

1. Asegúrate de que el entorno virtual Python está activado:

   - Linux y Mac:
   ```bash
   source venv/bin/activate
   ```

   - Windows:
   ```cmd
   venv\Scripts\activate
   ```

2. Inicia la interfaz de Gradio:

   ```bash
   python test.py
   ```

3. Accede a la interfaz del chatbot en tu navegador en `http://127.0.0.1:7864`.

## Documentación de la API de OpenAI

Para obtener más información sobre la API OpenAI y cómo utilizarla en sus proyectos, consulte la [Documentación de la API de OpenAI](https://platform.openai.com/docs/api-reference/).

## Licencia

Este proyecto está bajo licencia [GPL-3.0 License](LICENSE).