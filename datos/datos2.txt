GLOSARIO DE TÉRMINOS AGROPECUÁRIOS Y ACUÍCOLAS

ACAMADO: Diseño mecánico para formar la cama de siembra, con la pendiente requerida para 
evitar encharcamientos sobre la misma.

AFTOSA: Enfermedad viral muy contagiosa que afecta a animales de doble pezuña. 
Se caracteriza por fiebre y ampollas en boca, nariz, pezones y patas.

AGROQUÍMICOS: Concentrado de productos químicos agrícolas, es un producto químico utilizado 
en la agricultura. En la mayoría de los casos, agroquímicos se refiere a pesticidas que 
incluyen insecticidas, herbicidas, fungicidas y nematicidas.

AIREADOR: Herramienta que permite que el agua pueda moverse, desde el fondo sin remover sólidos 
y a su vez realiza un proceso de desgasificación.

ARADO: Implemento agrícola, puede ser de discos, vertedera u otro tipo, y sirve para abrir la 
capa arable del suelo antes y durante el ciclo de cultivo.

ÁREA PRODUCTIVA: Área destinada exclusivamente a la producción primaria, excluye caminos internos 
o áreas no aptas para el desarrollo de la actividad directa de producción.

ASPERSIÓN: Sistema de riego a presión que implica una lluvia uniforme cuyo objetivo es la 
infiltración en el mismo punto donde caen las gotas.

AVES: Animales vertebrados que se reproduce por medio del huevo, que están revestido de plumas 
y cuyos miembros anteriores están transformados en alas. Se refiere a las aves de corral como: 
pollos, gallinas, patos, pavos, cuya finalidad zootécnica es la producción de carne y huevo 
para el consumo humano.

BALANCEADO: Combinación de productos destinados a satisfacer las necesidades diarias de nutrientes. 
Es un producto seco, de diferentes formas y tamaños de partícula según la especie animal que lo 
consume, las harinas animales y vegetales son el mayor componente.

BIFÁSICO: Distribución de las fases de crecimiento del camarón en dos unidades de producción 
independientes (diferentes piscinas para pre cría y engorde).

BIOENERGÉTICOS: Aprovechamiento de la biomasa –específicamente los residuos– para su conversión 
en energía eléctrica, energía calorífica o biocombustibles. La biomasa se define como todo material 
orgánico formado por la vía biológica, y es de gran interés debido a que es muy abundante en el planeta.

BOMBA DE RECIRCULACIÓN: Son instalaciones de acuicultura en tierra que reutilizan el agua mediante un 
ciclo a través de un sistema de filtración para que pueda volver a utilizarse.

BRUCELOSIS: Enfermedad bacteriana, infectocontagiosa, producida por la Brucella abortus. Afecta 
principalmente a las hembras bovinas en edad reproductiva, provocando abortos y alteraciones reproductivas.

CÁMARA DE ENFRIAMIENTO: Cámara de refrigeración con un recinto aislado térmicamente dentro del cual se 
contiene materia para extraer su energía térmica. Esta extracción de energía se realiza por medio de un 
sistema de refrigeración. Su principal aplicación es la conservación de productos.

CAMPAMENTO: Instalación eventual en terreno abierto para uso de personas que van de camino o que se 
reúnen para un fin especial.

CAPACIDAD INSTALADA: Es el potencial de producción o volumen máximo de producción que una maquinaria 
o unidad puede producir en función de una especificación técnica. CARDUMEN Conjunto de peces similares 
que se desplazan juntos.

CASA COMERCIAL: Local comercial de venta al detalle, es decir de comercialización masiva de productos 
uniformes a grandes cantidades de clientes.

CELO O ESTRO: Periodo durante el cual las hembras de la clase mamíferos están receptivas sexualmente. 
Durante esta etapa ocurre la ovulación. Este suceso es diferente en cada una de las especies, 
pues varía la duración.

CERDA GESTANTE: Gestación confirmada en la cerda.

CERTIFICACIONES: Hace referencia a lo que técnicamente se denomina "evaluación de la conformidad”, 
esta evaluación es llevada a cabo para determinar si un producto, proceso o sistema de gestión cumple 
unos requisitos específicos nacionales y/o internacionales.

CICLO CORTO: Producción y cosecha de cultivos o plantas en un tiempo menor a un año; siempre es 
necesario comenzar de nuevo con la siembra o trasplante.

CICLOS DE PRODUCCIÓN: Es el período de meses que resulta de la suma de las siembras y cosechas 
que se realizan en los ciclos agrícolas invierno y verano, comprendido desde el noviembre de 
un año determinado al mes de abril del siguiente y de cosechas de productos perennes.

COMERCIALIZACIÓN: Acciones, estrategias, técnicas, métodos y decisiones que tienen como objetivo 
la venta de un producto dentro del mercado, siempre con la finalidad de obtener mejores resultados.

CONCENTRADO: Alimentos que aportan una alta concentración de nutrientes utilizables por el animal 
por unidad de volumen. Suelen ser alimentos con bajo contenido en fibra (menor del 18% Fibra Bruta).

CONSECIÓN: Acto administrativo mediante el cual una institución pública otorga a una persona los 
derechos de uso y goce, por un plazo de tiempo definido.

CONTRATO: Acuerdo, generalmente escrito, por el que dos o más partes se comprometen recíprocamente 
a respetar y cumplir una serie de condiciones.

CONVERSIÓN ALIMENTICIA: Cantidad o unidades de alimento que se debe consumir por animal para 
producir una unidad de producto.

COSECHA: Acción de desprender el fruto de la planta, semillas u hortalizas con fines de 
aprovecharlo. Mientras el fruto permanezca en la planta, aunque esté fisiológicamente maduro, 
no se considerará efectuada la cosecha. Temporada en que se realiza la recolección de algún producto 
en el campo, puede ser manual o mecánica.

CUARENTENA ZOOSANITARIA: Mantenimiento de un grupo de animales en aislamiento, sin contacto 
directo o indirecto con otros animales, con el propósito de llevar a cabo observaciones por un 
período de tiempo especificado y si es apropiado, realizar pruebas y tratamientos de 
adaptabilidad al nuevo ambiente.

CULTIVAR: Labores o prácticas de beneficio a la tierra y a las plantas, para que se desarrollen 
y fructifiquen con cuidado del hombre.

CULTIVO: Toda clase de especie vegetal cultivada en un campo, generalmente con fines 
económicos: a) Cultivo anual, que tiene un ciclo de vida no mayor de un año b) Cultivo asociado, 
es el tipo de siembra en que se maneja más de una especie vegetal dentro del campo, en una forma 
mezclada o intercalada que hace imposible su separación física en cuanto a superficie para cada uno 
de ellos y que además, los cultivos en asociación compiten por el espacio físico, luz y nutrientes, 
puesto que su desarrollo fisiológico es simultáneo c) Cultivo intercalado, práctica agrícola de 
aprovechamiento del suelo que comprende diferentes alternativas, predominando entre los cíclicos la 
siembra de uno o más surcos de un cultivo, alternando con otro o más surcos de otro cultivo. 
También se práctica el intercalamiento de cíclico entre frutales y plantaciones perennes, como plátano-maíz; 
caña de azúcar-frijol, durazno-maíz, mango-melón. De la misma manera, se da entre perennes como 
café-naranja, cocotero-limón, manzana-pera, cocoteroplátano, etc. d) Cultivo perenne, se refiere a 
frutales y plantaciones con vida económicamente útil de 2 a 30 años, aunque vegetativamente existen 
algunas especies que pueden durar más años. e) Cultivo solo o monocultivo, es el tipo de siembra en 
que se maneja una sola especie vegetal dentro del campo, por lo menos hasta el momento de su madurez 
fisiológica, o sea, cuando el fruto (grano) está completamente maduro.

DENSIDAD DE SIEMBRA: Número de plantas o animales por unidad de área de terreno.

DESCARTE: Eliminación o salida de animales de un grupo de producción, cualquiera sea su motivo, 
por ejemplo: edad, productividad, enfermedad, temperamento, etc.

DESTETE: Acción de retirar al animal de la madre y suplir la leche por alimento sólido.

DIETA: Conjunto de alimentos que consume habitualmente cada animal. E. Coli Es una de las principales 
enfermedades de la industria porcina y constituye una típica enfermedad bacteriana causada por 
Escherichia coli (E. coli) patógena. Fundamentalmente causa enfermedad y muerte en lechones recién 
nacidos y recientemente destetados.

EMBARCACIÓN: Construcción capaz de flotar, de ser dirigida por el hombre y propulsada por el 
viento u otro procedimiento.

EMBARCACIÓN RELACIONADA: Embarcación perteneciente a un grupo económico.

ENCALAMIENTO O ENCALADO: Aplicación de cal en pequeñas dosis y a intervalos frecuentes a los estanques 
de camarón en intentos de: regular la abundancia del fitoplancton, corrección de pH, prevenir propagación 
de enfermedades y mejorar condiciones de suelos enlodados.

ENGORDE ANIMAL: Animal que se cría única y exclusivamente para obtención de carne.

ENMIENDA AGRÍCOLA: Producto orgánico o mineral que se adiciona a un suelo para la corrección y mejora de 
al menos una condición física, química o biológica del mismo, de forma tal que las nuevas condiciones sean 
más adecuadas para las plantas sembradas (o por sembrar) en éste.

ESPECIE: Tipo de peces que según su tipo conforman una clase.

ESPEJO DE AGUA: Área destinada que se logra hacer con el agua de la piscina o estanque.

ESTABULADO O INTENSIVO: Encierro total del ganado, los animales están atados permanentemente en sus plazas 
y no pueden moverse libremente.

ESTACIÓN REBOMBEO: Equipo que permite la captación y transporte de agua para su uso.

EXPORTACIÓN: Actividad comercial que consiste en vender productos y servicios a otro país.

FANGUEO: Acción de preparación del suelo inundado con una pequeña capa de agua, generalmente 
con destino a la siembra de arroz. Se utilizan ruedas especiales tipo jaula (fangueadoras) para 
batido del lodo, con lo cual se destruyen los terrones y a la vez se favorece la nivelación del campo.

FERTILIZACIÓN EDÁFICA O RADICULAR: Fertilización realizada mediante aplicaciones directas al suelo y 
nutrición a planta a través de la raíz.

FERTILIZACIÓN FOLIAR: Fertilización realizada por la parte aérea de la planta (tallos, hojas), corrige 
carencias rápidamente y los resultados son inmediatos.

FERTILIZANTE: Los fertilizantes son sustancias ricas en nutrientes orgánicos o inorgánicos que se utilizan 
para mejorar las características del suelo para un mayor desarrollo de los cultivos agrícolas.

FERTILIZAR: Acción de aportar los nutrientes que la planta necesita para que sea plenamente productiva en 
cantidad y en calidad, es decir, es mejorar las carencias de micronutrientes para aumentar la rentabilidad 
de los cultivos. Para lograrlo, los fertilizantes deben aplicarse atendiendo a las necesidades reales de 
la planta, en la dosis adecuada, en el momento oportuno, y de la forma más efectiva.

FERTIRRIEGO: Técnica de fertilización que utiliza agua de riego para suministrar nutrientes al suelo 
cultivado. Esta aplicación se realiza a través del sistema de riego más conveniente para el cultivo, utilizando 
técnicas como goteo o aspersión.

FINCA: Hacienda o propiedad agrícola de gran extensión.

FITOSANIDAD: Acción referente a la Parasitología Agrícola. Se abordan temas sobre insectos, hongos, bacterias, 
nematos, virus y ácaros fitopatógenos.

FÓRMULA ANIMAL: Productos dietéticos constituidos por una mezcla o receta definida de macro y micronutrientes.

FORRAJE: Alimento de origen vegetal cultivado por el hombre y servido en pie a los herbívoros. Entre otros tenemos 
al heno, paja, pastos y silos; contiene gran cantidad de fibra o celulosa y es de escaso valor nutritivo.

GANADO: Conjunto de animales domésticos que aprovecha el hombre para obtener leche, carne, piel, lana, etc., 
para que lo ayude a realizar trabajos fuertes o para que lo traslade de un lugar a otro. Existen diferentes 
especies: bovino o vacuno, bravo o de lidia, caprino o cabrío, equino o caballar, ovino o lanar y porcino.

GESTACIÓN: Período de tiempo comprendido entre la concepción y el nacimiento.

GRIPE PORCINA: Enfermedad viral respiratoria de los cerdos causada por el virus de influenza tipo A. Pueden 
causar altos niveles de enfermedad en piaras de cerdos pero causan pocas muertes. Los síntomas pueden incluir 
fiebre, depresión, tos (tos perruna), secreción nasal y ocular, estornudos, dificultades para respirar, 
enrojecimiento o inflamación de los ojos y dejar de comer. No obstante, algunos cerdos infectados con el virus 
de la influenza pueden no presentar ningún síntoma de la enfermedad.

HATO: Conjunto de animales, sea de ganado mayor o menor: ganadero, lechero. Manada o rebaño.

HECTÁREA: Medida de superficie equivalente a 10,000 m² (metros cuadrados).

HIDRÓFONO: Elemento parte de tecnología de alimentación de retroalimentación acústica. Es un dispositivo sumergible 
en agua que capta el sonido generado del camarón cuando consume alimento.

INDUSTRIALIZACIÓN: Producción de bienes y servicios a gran escala, mediante la utilización de máquinas accionadas 
por fuentes de energía diversas.

INSEMINACIÓN ARTIFICIAL: Colocación de una muestra de semen, previamente preparada en el laboratorio, en el interior 
del útero de una hembra con el fin de incrementar el potencial de los espermatozoides y las posibilidades de 
fecundación del óvulo.

LARVA: Fases juvenil de los animales con anatomía, fisiología y ecología diferente del adulto.

LICENCIA AMBIENTAL: Permiso ambiental obligatorio para obras, actividades o proyectos de mediano o alto impacto 
ambiental, otorgada por la autoridad ambiental competente, mediante el Sistema Único de Información Ambiental (SUIA).

LÍNEA GENÉTICA: Raza producto del cruce de varias razas para una mejor producción.

MALEZAS: La maleza, mala hierba, hierba mala, monte o planta indeseable es cualquier planta que crece de forma 
silvestre en una zona cultivada o controlada por el ser humano como cultivos agrícolas.

MANEJO FITOSANITARIO: Métodos y técnicas para la prevención, control, eliminación o curación de las enfermedades 
de las plantas, procurando la estabilidad y bienestar del agroecosistema.

MECANIZACIÓN: Uso de cualquier medio auxiliar mecánico para realizar una tarea u operación relacionada con la 
producción agrícola. Según su forma de accionamiento puede ser: manual, animal o motorizado.

MERMA: Pérdida de alguna de las características físicas de los productos obtenidos o, mejor, de alguno de los 
factores utilizados para su obtención: peso, volumen, longitud, etc. También, es la disminución o reducción 
de una cierta cantidad total original. MÉTODO Proceso o camino sistemático establecido para realizar una tarea 
o trabajo con el fin de alcanzar un objetivo predeterminado.

MICROASPERSIÓN: Aplicación de agua en forma de lluvia fina mediante dispositivos (llamados micro aspersores) 
que la distribuyen en un radio no superior a los 3 metros.

MONTA DIRECTA: Monta natural o forma tradicional del contacto sexual entre el macho y la hembra.

MURO CARROZABLE: Estructura de contención rígida destinada a contener y en su parte superior pueden circular autos.

NAUPLIO: Larva nauplio o nauplius, es la primera larva característica de los crustáceos. Es objeto de cultivo o 
se utiliza en alimentación de peces en la fase alevín. NIVELACIÓN LASER Modificación del relieve o micro relieve de 
la superficie del terreno, uniformizándolo y adecuándolo previo a siembra.

NUTRICIÓN ANIMAL O ALIMENTACIÓN ANIMAL: es el estudio de reacciones bioquímicas y procesos fisiológicos que sufre 
el alimento en el organismo animal para transformarse en leche, carne, trabajo, etc. y que a su vez permite que los 
animales expresen al máximo su potencial genético.

ORDEÑO ANIMAL: Acción de extraer la leche de las glándulas mamarias de animales en periodo de lactación. Se puede 
realizar de forma manual o mecánica.

ORDEÑO AUTOMÁTICO O MECÁNICO: Es la extracción rápida y completa de leche sin causar daños al pezón y al tejido 
mamario. La extracción se realiza en un establo o sala de ordeño y toda la leche es enviada directamente a un tanque de 
enfriamiento para su posterior entrega.

ORDEÑO MANUAL: Se lleva a cabo masajeando, presionando y tirando del pezón hacia abajo hasta que sale la leche que se 
recoge en un recipiente, normalmente un cubo o balde.

ORDEÑO SEMIAUTOMÁTICO O PORTÁTIL: Es la extracción rápida y completa de leche sin causar daños al pezón y al tejido mamario, 
la extracción se puede realizar en diferentes puntos y la leche es depositada en taque simple sin enfriamiento.

OVERHAUL: Tratamiento en profundidad de cada pieza del motor con la finalidad de afinarlo y dejarlo en óptimas condiciones.

PASTOREO: Método de cría de animales en el que se permite que el ganado doméstico deambule al aire libre y consuma 
vegetación silvestre.

PASTURA: Pasto o hierba para el ganado.

PELÁGICOS: Animales acuáticos vertebrados que habitan en aguas medias o cerca de la superficie, en conglomerados denominados 
cardúmenes, y entre sus componentes encontramos: pinchagua, sardinas, macarela y que se constituyen en las especies 
principales de la actividad de transformación para la producción de harina de pescado, enlatado y aceites.

PELLET O PÉLET: Denominación genérica utilizada para referirse a pequeñas porciones de material alimenticio. 

PERIODICIDAD Frecuencia con la que aparece, sucede o se realiza una cosa repetitiva.

PESCA DE ANZUELO o PALANGRE: es un tipo de pesca que consiste en lanzar una línea con miles de anzuelos al mar, 
se trata de una pesca selectiva y limpia de especies específicas.

PESCA DE ARRASTRE: Método no selectivo ampliamente utilizado para capturar peces e invertebrados marinos. Consiste en el 
arrastre de una red a través del agua detrás de una o más embarcaciones.

PESCA DE CERCO: Actividad pesquera que se ejerce con una red de forma rectangular que captura los peces rodeándolos y se 
cierra en forma de bolsa por la parte inferior.

PESO OBJETIVO: Unidad de peso aspiracional por unidad animal al tiempo de cosecha.

PIARA: Conjunto de cerdos.

PISCINAS ACUÍCOLAS: Recinto artificial excavado en tierra con poca profundidad y con sistema de desagüe para poder realizar 
recambios parciales de agua o vaciado total.

PLAGAS: Las plagas son plantas, animales, insectos, microbios u otros organismos no deseados que interfieren con la actividad 
humana y económicamente tendrían el impacto de generar pérdida. Estos pueden morder, destruir cultivos de alimentos, dañar propiedad.

PLANTAS: Seres vivos vegetales de diferentes tipos o familias, algunas pueden alimentarse y crecer solas. Para fines comerciales 
es necesaria la intervención del hombre en su proceso de producción.

POSTURA: Actividad avícola de producción de huevos.

POTRERO: Parte de terreno generalmente cercado, donde se siembran pastos destinados a la alimentación de ganado. Terreno cercado 
donde se cría todo tipo de ganado.

PREDIO AGRÍCOLA: Extensión de terreno para la obtención de productos agropecuarios primarios, tal como: producción de cultivos, 
animales y especies acuícolas.

PROCESAMIENTO: Acción de someter uno o varios insumos a un proceso de elaboración y transformación.

PROCESO: Conjunto de actividades enlazadas entre sí que, partiendo de uno o más inputs (entradas) los transforma, 
generando un output (resultado).

PRODUCCIÓN: Es la actividad principal de cualquier sistema económico que está organizado precisamente para producir, 
distribuir y consumir los bienes y servicios necesarios para la satisfacción de las necesidades humanas. a) Producción agrícola. 
Es una cantidad de producto primario, que se obtiene mediante el uso de recursos como tierra, mano de obra y tecnología, 
a través de la siembra de cultivos en el período de referencia b) Producción forestal. Se refiere a la explotación, mantenimiento, 
reforestación y tala de árboles c) Producción pecuaria. Comprende la actividad de crianza de animales y la explotación de los 
bienes, fruto de su cuidado, durante un período de referencia d) Producción pesquera. Extracción, captura, colección o cultivo de 
especies biológicas o elementos biogénicos cuyo medio de vida total, parcial o temporal sea el agua, así como los actos previos o 
posteriores relacionados con ella. e) Producción real. Es la producción medida en unidades físicas o la producción valorada en 
dinero restándole el efecto de los precios.

RACEWAYS: Sistema de flujo continuo que cuenta con un flujo rápido de agua que permite mantener una biomasa elevada de organismos 
y un recambio de agua continúo. RALEO Práctica cultural que consiste en la eliminación de animales, cultivos o frutos por un 
exceso o necesidad económica.

RATIO o TASA DE CONVERSIÓN (BANANO): Cantidad de racimos que se requieren para elaborar una caja de banano de 20 kg. El tipo de 
producción orgánica o convencional será un factor importante a considerar en esta tasa.

RAZA: Cada uno de los grupos en que se subdividen alguna especie vegetal y animal y que se refieren a características que las 
diferencian y que subsisten por herencia.

RENDIMIENTO: Producto o utilidad de una cosa, en agricultura y economía agraria, rendimiento de la tierra o rendimiento agrícola 
es la producción dividida entre la superficie.

RENOVACIÓN DE PASTURA: Acción de renovar pasturas degradadas para recuperar la capacidad de producción de forraje e incrementar 
la capacidad de carga, con un impacto significativo en respuesta productiva de animales y beneficio económico.

REPRODUCTORES: Animal con condiciones genéticas deseables para perpetuar la especie. RESIDUOS Materiales o productos que se desechan 
ya sea en estado sólido, semisólido, líquido o gaseoso, que se contienen en recipientes o depósitos, y que necesitan estar sujetos 
a tratamiento o disposición final.

RIEGO: Acción o efecto de regar agua. Consiste en aplicar agua al suelo cultivado, con el objeto de suplir la insuficiencia o la 
falta de oportunidad de agua de lluvia.

RIEGO POR GOTEO: Conocido como riego gota a gota, consiste en regar las plantas por medio de gotas de agua las cuales son filtradas 
por una manguera o tubo.

RIEGO POR GRAVEDAD: La distribución del agua es por gravedad. Al avanzar el agua sobre la superficie del suelo se produce 
simultáneamente la distribución del agua en la parcela y la infiltración de la misma en el perfil del suelo.

ROMPLOW o RASTRA DE TIRO: Equipo que incorpora la materia orgánica (resto de cosecha, el abono, las malas hierbas, etc.) en la 
superficie del suelo y reduce la erosión del viento y del agua.

ROTOVATOR: Máquina agrícola que se utilizan para el corte, remoción y pulverización acelerada del suelo. Constan de un eje horizontal 
de cuchillas curvas rotativas, que tras su paso dejan una cama ideal para la siembra de semillas.

SALMUERA: Agua cargada con sal utilizada para la conservación.

SALOBRE: Agua que tiene más salinidad que el agua dulce, pero no tanto como el agua de mar. SALUD ANIMAL O bienestar animal, estudia 
aspectos relacionados con la salud física, el estado emocional y el comportamiento de los animales.

SEMI ESTABULADO: Encierro parcial del ganado en un corral para brindar mejor cuido y alimentación. Implica la construcción o habilitación 
de un espacio para el confinamiento de los animales.

SEMILLA CERTIFICADA: Semilla que brinda certeza al agricultor, garantiza calidad física, genética, sanitaria y fisiológica, otorgando 
al consumidor una evidencia sobre el insumo que está adquiriendo por medio de un certificado de calidad (etiqueta de certificación).

SEMILLA: Producto del óvulo fecundado y envuelto por una cubierta protectora que, sembrado en condiciones adecuadas, da origen a una 
nueva planta. Por extensión, pedazo de una planta provisto de yemas, que es también medio de reproducción. SPOT Comercio de mercado 
abierto en función de oferta y demanda.

SUBSOLADO: Acción de remover el suelo por debajo de la capa arable, o roturación a bastante profundidad sin voltear la tierra. 
Realizado por un subsolador compuesto por un número impar de brazos o púas robustas y rígidas para trabajar en suelo endurecido, 
montados sobre un bastidor capaz de soportar estos esfuerzos.

SUPLEMENTO ALIMENTICIO: Acción de compensar las deficiencias de raciones ingeridas, mediante la adición de suplementos ricos en 
energía y minerales. SURCADO Labor de trazar surcos paralelos dentro de un lote espaciados a una misma distancia donde se siembra la 
semilla o material vegetativo.

TALUD: Pendiente lateral o parte inclinada de los diques o muros, está dada por la altura del dique y el ancho de la base.

TERNERAS: Cría de la vaca desde los cuatro días de nacida hasta los seis meses de edad y que por lo general se encuentra en 
período de lactancia. En ganado de leche su potencial es reemplazar a vacas de descarte.

TERNEROS: Cría de la vaca desde los cuatro días de nacido hasta los seis meses de edad y que por lo general se encuentra en 
período de lactancia. Con potencial reproductor o ganado de carne.

TIMER: Técnica de manejo de alimento por temporizador.

TRANSFERENCIA DE EMBRIONES O FECUNDACIÓN IN VITRO: es una técnica mediante la cual, los embriones (óvulos fertilizados) son 
colectados del cuerno uterino de la hembra antes de la nidación (donadora), y transferidos al cuerno uterino de otras hembras 
para completar su gestación (receptoras). Es posible que el proceso de fecundación para generación de embriones se 
desarrolle en laboratorios.

TRIFÁSICO: Distribución de las fases de crecimiento del camarón en tres unidades de producción independientes (diferentes 
piscinas para pre cría, pre engorde y engorde) TÚNIDOS Pez óseo marino con presencia de dos aletas dorsales, generalmente bien 
separadas, la primera soportada por espinas y la segunda por rayas blandas. Las principales especies son: atún rojo, atún de 
aleta amarilla, albacora y bonito.

VARIEDAD: Grupo de plantas definido con mayor precisión, seleccionado dentro de una especie, que presentan una serie de características 
comunes, es decir, es seleccionada por la naturaleza, en respuesta a cambios de factores ambientales.

VEDA: Tiempo durante el cual está prohibido cazar o pescar en un determinado lugar o una determinada especie.

ZOOTECNISTA: Persona con capacidad de observar y analizar la crianza de un animal de consumo humano.