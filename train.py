from llama_index import SimpleDirectoryReader, GPTVectorStoreIndex, LLMPredictor, PromptHelper, ServiceContext
import os, openai
from langchain import OpenAI
from dotenv import load_dotenv

load_dotenv('.env')
my_api_key = os.environ.get('OPENAI_API_KEY')
openai.api_key = my_api_key

max_input = 4098
tokens = 1024
chnk_size = 600
chnk_ovrlp_ratio = 0.75

def entrenamiento(path):
    docs = SimpleDirectoryReader(path).load_data()
    prompt_helper = PromptHelper(max_input,tokens,chunk_size_limit=chnk_size,chunk_overlap_ratio=chnk_ovrlp_ratio)
    modelo = LLMPredictor(llm=OpenAI(temperature=0,model_name='text-davinci-003',max_tokens=tokens)) # type: ignore
    contexto = ServiceContext.from_defaults(llm_predictor=modelo,prompt_helper=prompt_helper)
    index_model = GPTVectorStoreIndex.from_documents(docs,service_context=contexto)
    index_model.storage_context.persist('modelo')

entrenamiento('datos')