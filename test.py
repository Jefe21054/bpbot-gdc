from llama_index import StorageContext, load_index_from_storage
import os, openai
import gradio as gr
from gradio.themes.utils import colors, fonts
from dotenv import load_dotenv

load_dotenv('.env')
my_api_key = os.environ.get('OPENAI_API_KEY')
openai.api_key = my_api_key

with gr.Blocks(title='Banco Pichincha ChatBot',
                theme=gr.themes.Soft(primary_hue=colors.gray, font=[fonts.GoogleFont('Quicksand'), 
                                'ui-sans-serif', 'sans-serif']).set(loader_color='blue',
                                button_primary_background_fill='#0F265C',
                                button_primary_background_fill_dark='#0F265C',
                                button_primary_background_fill_hover='#FFDD00',
                                button_primary_background_fill_hover_dark='#FFDD00',
                                button_primary_border_color='#0F265C',
                                button_primary_border_color_dark='#0F265C',
                                button_primary_text_color='#FFDD00',
                                button_primary_text_color_dark='#FFDD00',
                                button_primary_text_color_hover='#0F265C',
                                button_primary_text_color_hover_dark='#0F265C',),
                                css='.gradio-container {background-color: #FFF}') as demo:

    gr.Markdown(
    '''
    <h1 style="text-align: center; color:#0f265c">Proyecto Trainees</h1>
    <img 
        style="display: block; 
                margin-left: auto;
                margin-right: auto;
                width: 30%;"
        src="https://www.pichincha.com/portal/Portals/0/MainPichincha.svg" 
        alt="BP">
    </img>
    <h2 style="text-align: center; color:#0f265c">Futuros Líderes - Generación del Cambio</h2>
    <h3 style="text-align: center; color:#0f265c">Autores: Iván Iglesias, Andrés Muñoz, Jessica Oppikofer, Isabel Salguero</h3>
    '''
    )
    with gr.Row():
        inputs = gr.Textbox(lines=5,label='Ingresa por favor tu pregunta')
        outputs=gr.Textbox(lines=5,label='Respuesta',show_copy_button=True)
    with gr.Row():
        with gr.Column(scale=1):
            button = gr.Button('Enviar', variant='primary',scale=1)
        with gr.Column(scale=1):
            clear = gr.Button('Limpiar',variant='primary',scale=1)

    def chatbot(textoentrada):
        # rebuild storage context
        storage_context = StorageContext.from_defaults(persist_dir='modelo')
        modelo = load_index_from_storage(storage_context)
        query_engine = modelo.as_query_engine()
        respuesta = query_engine.query(textoentrada)
        return respuesta
    
    def limpiar():
        # clear function
        return None,None

    button.click(chatbot, inputs, outputs)
    clear.click(fn=limpiar, inputs=None, outputs=[inputs, outputs])

demo.launch(favicon_path='static/favicon.ico', share=True)